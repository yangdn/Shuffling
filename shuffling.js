# shuffling
shuffling  轮播插件
/**
 * Created by 杨东楠
 * @shuffling  调用参数5;
 * @ele 轮播图片
 * @eleA 轮播按钮
 * @elebtn 轮播左右点击按钮
 * @eleAttr 轮播图片 层级显示
 * @eleAattr 轮播按钮 样式
 */
function shuffling(ele,eleA,eleBtn,elaAttr,eleAattr) {
  var  time = setInterval(judge,2000),
       Li = document.getElementsByTagName(ele),
       A = document.getElementsByTagName(eleA),
       Btn = document.getElementsByTagName(eleBtn),
       n = 0;
    //定时器右侧方向和点击右侧
    function judge() {
        n++;
        if( n > 4 ){
            n = 1;
        }
        removeClass();
        Li[n-1].className = elaAttr;
        A[n-1].className = eleAattr;
    }
    //点击按钮控制方向左侧
    function judgeZ() {
        n--;
        if( n < 1 ){
            n = 4;
        }
        removeClass();
        Li[n-1].className = elaAttr;
        A[n-1].className = eleAattr;
    }
    for(var j=0;j<Btn.length;j++){
        addEvent(Btn[j],'mousemove',oMover);
        addEvent(Btn[j],'mouseout',oMout);
    }
    addEvent(Btn[0],'click',judgeZ);
    addEvent(Btn[1],'click',judge);
    function removeClass() { //初始化页面元素
        for (var i=0;i<A.length;i++){
            var i = i;
            Li[i].className = ' ';
            A[i].className = ' ';
            A[i].indexs = i;
            //监听A元素事件
            addEvent(A[i],'mousemove',oMover )
            addEvent(A[i],'mouseout',oMout);
            addEvent(A[i],'click',liaClick);
        }
    }
    function oMover() {
        clearInterval(time);
    } //鼠标悬停
    function oMout() {
        time = setInterval(judge,2000);
    } //鼠标离开
    function liaClick() {
        removeClass();
        this.className =eleAattr;
        Li[this.indexs].className = elaAttr;
        n = this.indexs+1
    } //点击A 找到当前的图片
    function addEvent(ele, type, func) {
        if( ele.addEventListener )
            ele.addEventListener( type, func, false );
        else
            ele.attachEvent( 'on'+type, func );
    } //监听事件兼容IE8
}
